Whatsappshare Module
=======================================
Whatsappshare module provide a link to share your page and custom text on every node title page.
Once enable the module then go to /admin/config/whatsappshare page and update the whatsappshare configruation.
Whatsappshare module provides a admin page to customized -
1. Text(Share) which will appear after title on every node page.
2. An option to manage the size of whatsapp share button size.
3. The custom text which has to be share.

Note: 
Sharing location using a jQuery selector field:- example: #page-title or .site-branding__logo to place it after the page title or after logo (dependeds on availblity of that selector in your active theme).


Original idea is inherited from http://whatsapp-sharing.com/ 

FAQ

1. Everything is configured properly still I am not getting whatsapp share button.
Please check on mobile device bro! Button will not appear in PC browser.
Javascript will detect the device automatic and render the link.

2. Can I add custom CSS and JS.
Try it yourself. Currently it is tested on default theme only.

3. Share button doesn't appare on any page
You have to configure the same then only it will start apparing 

4. Can I move the whatsapp share link on the page.
Yes, Now  7.x-2.0 version onwards, module  is having option to update the location where you want to move whatsapp share button from /admin/config/services/whatsappshare. By default it will render after your title on every page.

5. Can I insert class also on the place of id from admin panel.
Yes, you can update class as well, ex ".mycustomclass".
